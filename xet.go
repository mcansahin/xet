package xet

import (
	"fmt"
	"os"
)

var (
	path string
)

func File(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		var file, err = os.Create(path)
		checkError(err)
		defer file.Close()
	}
}

func Set() {
	fmt.Println(path)
}

func checkError(err error) {
	if err != nil {
		fmt.Println(err.Error())
	}
}
